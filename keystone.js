var keystone = require('keystone')
const svelteViewEngine = require("svelte-view-engine");

const engine = 'html'
const dir = 'templates/views'

const render = svelteViewEngine({
    template: "./templates/template.html", // see Root template below
    dir: 'templates/views',
    type: engine,
    init: true,
    watch: true,
    liveReload: true,
    svelte: {
        // rollup-plugin-svelte config
    },
})

// pass initiation options here
keystone.init({
    'cookie secret': 'secure string goes here',
    'name': 'Arius',
    'user model': 'User',
    'auto update': true,
    'auth': true,
    'views': dir,
    'mongo': `mongodb://joma:huipizda228322@localhost:27017/arius`,
    'view engine': engine,
    'custom engine': render,
    'static': 'public'
});

keystone.import('models')

keystone.set('routes', require('./routes'));

keystone.start();
