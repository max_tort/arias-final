function routeFunction(req, res) {
    res.redirect('/index.html')
  }
  
  module.exports = function (app) {
    app.get('/', routeFunction);
  };